from shape import Shape
import math


class Octagon(Shape):
    '''
    Constructor for class Octagon.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Octagon", length, length)
    '''
    Perimeter of the Octagon = 8 * length
    '''
    def get_perimeter(self):
        return 8 * self.lenght
    '''
    Area of the the Octagon: 2 * (1 + root(2)) * length^2
    '''
    def get_area(self):
        return(2 * (1 + math.pow(2, 0.5)) * math.pow(self.length, 2))

    def __repr__(self):
        return("Octagon with lenght:{}".format(self.length))
