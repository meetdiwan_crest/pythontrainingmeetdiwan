from shape import Shape


class Square(Shape):
    '''
    Constructor for class Square.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Square", length, length)
    '''
    Perimeter of the Sqaure = 4 * length.
    '''
    def get_perimeter(self):
        return 4 * self.length
    '''
    Area of the Circle: length * length
    '''
    def get_area(self):
        return self.length * self.length

    def __repr__(self):
        return("Square has length of {}".format(self.length))
