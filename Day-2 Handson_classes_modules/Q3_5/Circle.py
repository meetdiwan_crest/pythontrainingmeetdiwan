from shape import Shape
import math


class Circle(Shape):
    '''
    Constructor for class Circle.
    '''
    def __init__(self, radius):
        self.radius = radius
        super().__init__(self, radius, radius)
    '''
    Perimeter of the Circle = 2*pi*r
    '''
    def get_perimeter(self):
        return 2 * math.pi * self.radius
    '''
    Area of the Circle: pi*r*r
    '''
    def get_area(self):
        return math.pi * self.radius**2

    def __repr__(self):
        return("Circle has radius of {}".format(self.radius))
