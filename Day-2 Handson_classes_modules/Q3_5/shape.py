'''
Create a class Shape with __init__, __repr__, __eq__, __gt__, __lt__ functions.
These functions should raise NotImplementedError when called with
an object of Shape class.
Add valid docstrings containing description, and usage, parameter
and return details as applicable.
'''


class Shape:
    def __init__(self, name, length, bredth):
        self.name = name
        self.length = length
        self.bredth = bredth

    def __eg__(self, shape):
        return NotImplementedError()

    def __gt__(self, shape):
        return NotImplementedError()

    def __lt__(self, shape):
        return NotImplementedError()

    def __repr__(self):
        return (
            "Shape of {}: length:{} bredth:{}".format(self.name,
                                                      self.length, self.bredth)
        )


if __name__ == '__main__':
    rectangle = Shape("Rectangle", 100, 50)
    print(repr(rectangle))
