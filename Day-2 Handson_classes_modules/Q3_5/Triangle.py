from shape import Shape
import math


class Triangle(Shape):
    '''
    Constructor for class Triangle.
    '''
    def __init__(self, length1, length2, length3):
        self.length1 = length1
        self.length2 = length2
        self.length3 = length3
        super().__init__(self, length1, length2)
    '''
    Perimeter of the Traingle = length of side1
                                + length of side2
                                + lenght of side3
    '''
    def get_perimeter(self):
        return(
            self.length1
            + self.length2
            + self.length3)
    '''
    Area of the the Triangle:
    p = perimeter / 2
    Area = (p*(p - l1)*(p - l2)*(p - l3))^0.5
    '''
    def get_area(self):
        half_perimeter = self.get_perimeter() / 2
        return math.pow(half_perimeter * (half_perimeter - self.length1)
                        * (half_perimeter - self.length2)
                        * (half_perimeter - self.length3), 0.5)

    def __repr__(self):
        return("Triangle with sides {}, {}, {}".format(self.length1,
                                                       self.length2,
                                                       self.length3))
