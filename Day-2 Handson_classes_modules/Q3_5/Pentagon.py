from shape import Shape


class Pentagon(Shape):
    '''
    Constructor for class Pentagon.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Pentagon", length, length)
    '''
    Perimeter of the Pentagon = 5 * length
    '''
    def get_perimeter(self):
        return 5 * self.length
    '''
    Area of the the Pentagon: perimeter * length / 2
    '''
    def get_area(self):
        perimeter = self.get_perimeter()
        return(perimeter * self.length / 2)

    def __repr__(self):
        return("Pentagon with side lenght:{}".format(self.length))
