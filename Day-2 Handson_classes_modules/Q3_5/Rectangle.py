from shape import Shape


class Rectangle(Shape):
    '''
    Constructor for class Rectangle.
    '''
    def __init__(self, length, bredth):
        self.length = length
        self.bredth = bredth
        super().__init__(self, length, bredth)
    '''
    Perimeter of the Sqaure = (2 * length) + (2 * bredth).
    '''
    def get_perimeter(self):
        return 2 * self.length + 2 * self.bredth
    '''
    Area of the the Square: length * bredth
    '''
    def get_area(self):
        return self.length * self.bredth

    def __repr__(self):
        return("Rectangle has length:{} bredth:{}".format(self.length,
                                                          self.bredth))
