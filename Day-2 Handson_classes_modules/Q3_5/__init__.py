from Circle import Circle
from Triangle import Triangle
from Square import Square
from Rectangle import Rectangle
from Pentagon import Pentagon
from Hexagon import Hexagon
from Heptagon import Heptagon
from Octagon import Octagon


def comparearea(shape1, shape2):
    '''
    Compares area of both shapes using get_area() method.
    '''
    if(shape1.get_area() > shape2.get_area()):
        print("First Object has higher area")
    elif(shape1.get_area() < shape2.get_area()):
        print("Second object has higher area")
    else:
        print("Both object has same area")


def compareperimeter(shape1, shape2):
    '''
    Compares Perimeter of both shapes using get_perimeter() method.
    '''
    if(shape1.get_perimeter() > shape2.get_perimeter()):
        print("First Object has higher perimeter")
    elif(shape1.get_perimeter() < shape2.get_perimeter()):
        print("Second object has higher perimeter")
    else:
        print("Both object has same perimeter")


def choose_shape():
    '''
    Takes choice from user.
    According to choice, takes parameters for constructor call.
    returns Objects.
    '''
    choice = int(input('''Enter your choice
                   1. Square
                   2. Rectangle
                   3. Circle
                   4. Triangle
                   5. Pentagon
                   6. Hexagon
                   7. Heptagon
                   8. Octagon:'''))
    if choice == 1:
        length = int(input("Enter length of the sqaure"))
        return Square(length)
    elif choice == 2:
        length = int(input("Enter length of the Rectangle"))
        bredth = int(input("Enter Bredth of the Rectangle"))
        return Rectangle(length, bredth)
    elif choice == 3:
        radius = int(input("Enter radius of the Circle"))
        return Circle(radius)
    elif choice == 4:
        length1 = int(input("Enter length of side1"))
        length2 = int(input("Enter length of side2"))
        length3 = int(input("Enter length of side3"))
        return Triangle(length1, length2, length3)
    elif choice == 5:
        length = int(input("Enter length of the Pentagon"))
        return Pentagon(length)
    elif choice == 6:
        length = int(input("Enter length of the Hexagon"))
        return Hexagon(length)
    elif choice == 7:
        length = int(input("Enter length of the Heptagon"))
        return Heptagon(length)
    elif choice == 8:
        length = int(input("Enter length of the Octagon"))
        return Octagon(length)
    else:
        return 'Incorrect choice'


if __name__ == '__main__':
    shape1 = choose_shape()
    shape2 = choose_shape()
    # compares Area of objects paased as parameter
    comparearea(shape1, shape2)
    # compares permitere of objects passed as parameter
    compareperimeter(shape1, shape2)
