from shape import Shape
import math


class Hexagon(Shape):
    '''
    Constructor for class Hexagon.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Hexagon", length, length)
    '''
    Perimeter of the Hexagon = 6 * length
    '''
    def get_perimeter(self):
        return(6 * self.length)
    '''
    Area of the the Hexagon: 3 * root(3) * length^2 /2
    '''
    def get_area(self):
        return(3 * math.pow(3, 0.5) * math.pow(self.length, 2) / 2)

    def __repr__(self):
        return("Hexaagon with side lenght:{}".format(self.length))
