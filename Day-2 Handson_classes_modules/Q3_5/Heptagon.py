from shape import Shape
import math


class Heptagon(Shape):
    '''
    Constructor for class Heptagon.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Heptagon", length, length)
    '''
    Perimeter of the Heptagon = 7 * length
    '''
    def get_perimeter(self):
        return 7 * self.length
    '''
    Area of the the Pentagon: 7 * length^2 * cos(pi/7) / 4 * sin(pi/7)
    '''
    def get_area(self):
        return(
            7 * math.pow(self.length, 2)
            * math.cos(math.pi / 7) / 4
            * math.sin(math.pi / 7)
        )

    def __repr__(self):
        return("Heptagon with length{}".format(self.length))
