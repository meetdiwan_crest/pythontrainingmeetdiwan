'''
Task4 : Implement a destructor of the objects which voids
the object of the Class
Task6: Extend task 4, get the lengths/breadths of the two classes
in command line arguments, validate the inputs
and perform the task as mentioned in task 4. Write the necessary docstrings.
'''


class Shape:
    '''
    Constructor for class Shape.
    Sets length & bredth for Shape object.
    '''
    def __init__(self, name, length, bredth):
        self.name = name
        self.length = length
        self.bredth = bredth

    # returns string when print method is called.
    def __str__(self):
        return (
            "Shape of {}: length:{} bredth:{}".format(self.name,
                                                      self.length, self.bredth)
        )

    # Destructor method for Shape class.
    def __del__(self):
        print("Destructor is called")


if __name__ == '__main__':
    # Takes length & bredth from user.
    length, bredth = int(input("Enter length")), int(input("Enter bredth"))
    # Create shape object from given user input
    shape1 = Shape("Rectangle", length, bredth)
    # __str__ method is called.
    print(shape1)
    # Destructor method is called.
    del(shape1)
