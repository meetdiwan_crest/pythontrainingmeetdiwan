'''
Create classes: Circle, Square, Rectangle, Triangle, Pentagon, Hexagon,
Heptagon, Octagon as child classes of Shape.
These classes should have functions to calculate perimeter, area, description
about the shape and comparison with another shape based on area or perimeter.
For eg: there is an object of Circle with x area and object of Heptagon
with y area, a function should tell which object has a larger area.
'''


from Q1 import Shape
import math


class Circle(Shape):
    '''
    Constructor for class Circle.
    '''
    def __init__(self, radius):
        self.radius = radius
        super().__init__(self, radius, radius)
    '''
    Perimeter of the Circle = 2*pi*r
    '''
    def get_perimeter(self):
        return 2 * math.pi * self.radius
    '''
    Area of the Circle: pi*r*r
    '''
    def get_area(self):
        return math.pi * self.radius**2

    def __repr__(self):
        return("Circle has radius of {}".format(self.radius))


class Square(Shape):
    '''
    Constructor for class Square.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Square", length, length)
    '''
    Perimeter of the Sqaure = 4 * length.
    '''
    def get_perimeter(self):
        return 4 * self.length
    '''
    Area of the Circle: length * length
    '''
    def get_area(self):
        return self.length * self.length

    def __repr__(self):
        return("Square has length of {}".format(self.length))


class Rectangle(Shape):
    '''
    Constructor for class Rectangle.
    '''
    def __init__(self, length, bredth):
        self.length = length
        self.bredth = bredth
        super().__init__(self, length, bredth)
    '''
    Perimeter of the Sqaure = (2 * length) + (2 * bredth).
    '''
    def get_perimeter(self):
        return 2 * self.length + 2 * self.bredth
    '''
    Area of the the Square: length * bredth
    '''
    def get_area(self):
        return self.length * self.bredth

    def __repr__(self):
        return("Rectangle has length:{} bredth:{}".format(self.length,
                                                          self.bredth))


class Triangle(Shape):
    '''
    Constructor for class Triangle.
    '''
    def __init__(self, length1, length2, length3):
        self.length1 = length1
        self.length2 = length2
        self.length3 = length3
        super().__init__(self, length1, length2)
    '''
    Perimeter of the Traingle = length of side1
                                + length of side2
                                + lenght of side3
    '''
    def get_perimeter(self):
        return(
            self.length1
            + self.length2
            + self.length3)
    '''
    Area of the the Triangle:
    p = perimeter / 2
    Area = (p*(p - l1)*(p - l2)*(p - l3))^0.5
    '''
    def get_area(self):
        half_perimeter = self.get_perimeter() / 2
        return math.pow(half_perimeter * (half_perimeter - self.length1)
                        * (half_perimeter - self.length2)
                        * (half_perimeter - self.length3), 0.5)

    def __repr__(self):
        return("Triangle with sides {}, {}, {}".format(self.length1,
                                                       self.length2,
                                                       self.length3))


class Pentagon(Shape):
    '''
    Constructor for class Pentagon.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Pentagon", length, length)
    '''
    Perimeter of the Pentagon = 5 * length
    '''
    def get_perimeter(self):
        return 5 * self.length
    '''
    Area of the the Pentagon: perimeter * length / 2
    '''
    def get_area(self):
        perimeter = self.get_perimeter()
        return(perimeter * self.length / 2)

    def __repr__(self):
        return("Pentagon with side lenght:{}".format(self.length))


class Hexagon(Shape):
    '''
    Constructor for class Hexagon.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Hexagon", length, length)
    '''
    Perimeter of the Hexagon = 6 * length
    '''
    def get_perimeter(self):
        return(6 * self.length)
    '''
    Area of the the Hexagon: 3 * root(3) * length^2 /2
    '''
    def get_area(self):
        return(3 * math.pow(3, 0.5) * math.pow(self.length, 2) / 2)

    def __repr__(self):
        return("Hexaagon with side lenght:{}".format(self.length))


class Heptagon(Shape):
    '''
    Constructor for class Heptagon.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Heptagon", length, length)
    '''
    Perimeter of the Heptagon = 7 * length
    '''
    def get_perimeter(self):
        return 7 * self.length
    '''
    Area of the the Heptagon: 7 * length^2 * cos(pi/7) / 4 * sin(pi/7)
    '''
    def get_area(self):
        return(
            7 * math.pow(self.length, 2)
            * math.cos(math.pi / 7) / 4
            * math.sin(math.pi / 7)
        )

    def __repr__(self):
        return("Heptagon with length{}".format(self.length))


class Octagon(Shape):
    '''
    Constructor for class Octagon.
    '''
    def __init__(self, length):
        self.length = length
        super().__init__("Octagon", length, length)
    '''
    Perimeter of the Octagon = 8 * length
    '''
    def get_perimeter(self):
        return 8 * self.lenght
    '''
    Area of the the Octagon: 2 * (1 + root(2)) * length^2
    '''
    def get_area(self):
        return(2 * (1 + math.pow(2, 0.5)) * math.pow(self.length, 2))

    def __repr__(self):
        return("Octagon with lenght:{}".format(self.length))


def comparearea(shape1, shape2):
    if(shape1.get_area() > shape2.get_area()):
        print("First Object has higher area")
    elif(shape1.get_area() < shape2.get_area()):
        print("Second object has higher area")
    else:
        print("Both object has same area")


def compareperimeter(shape1, shape2):
    if(shape1.get_perimeter() > shape2.get_perimeter()):
        print("First Object has higher perimeter")
    elif(shape1.get_perimeter() < shape2.get_perimeter()):
        print("Second object has higher perimeter")
    else:
        print("Both object has same perimeter")


if __name__ == '__main__':
    square1 = Square(40)
    rectangle1 = Rectangle(30, 40)
    comparearea(square1, rectangle1)
    pentagon1 = Pentagon(30)
    heptagon1 = Heptagon(20)
    compareperimeter(pentagon1, heptagon1)
