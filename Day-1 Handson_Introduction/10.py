'''
10. Write a Python function to insert a string in the
    middle of a string. For odd length of string,
    remove the middle character and replace with given string.
'''

input_string = input("Enter original string: ")

string_to_be_added = input("Enter string to be added: ")

if(len(input_string) % 2 == 0):
    print(input_string[:int(len(input_string)/2)]
          + string_to_be_added
          + input_string[int(len(input_string)/2):])
else:
    print(input_string[:int(len(input_string)/2)]
          + string_to_be_added
          + input_string[int(len(input_string)/2 + 1):])
