"""
1. Write python script to take no of arguments as input from the user.
    Then read no of arguments from the standard input.
    Print read arguments on output.

"""

number_of_argument = int(input("Enter number of arguments"))
arguments = []
for i in range(number_of_argument):
    argument = input("Enter argument {} :".format(i))
    arguments.append(argument)

# iterating through list to print elements of it.
for argument in arguments:
    print(argument)
