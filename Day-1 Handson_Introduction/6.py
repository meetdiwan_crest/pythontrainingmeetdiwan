'''
6. Write a python function which counts the frequency of-
    given character in a given string.
Inputs - A String A Character whose frequency needs to be determined
'''

input_string = input("Enter string: ")
input_character = input("Enter character: ")


def check_frequency(string1, input_character):
    temp_tuple = tuple(string1)
    # tuple.count(character) returns frequency of character inside tuple
    print("Frequency of {} is {}".format(input_character,
                                         temp_tuple.count(input_character)))


check_frequency(input_string, input_character)


def check_frequency2(string1, input_character):
    counter = 0
    # iterate through every character of string
    for i in string1:
        # if character matches than update the count by 1.
        if i == input_character:
            counter += 1
    print("Frequency of {} is {}".format(input_character, counter))


check_frequency2(input_string, input_character)
