'''
9. Write a Python function to remove the characters
   which have odd index
values of a given string.
'''

input_string = input("Enter string: ")
# Skipping every character at odd index
print(input_string[::2])
