'''
12. Read a sentence from the standard input.
    Find out how many times each word appear in given string.
      >>> Input :
      “This is a Python learning”
      >>> Output:
      >>> This 1
      >>> Is 1
      >>> a 1
      >>> Python 1
      >>> Learning 1
'''
sentence = input("Enter sentence: ")

# created dictionary to maintain the frequecny of the words.
frequency = {}

# split will break the sentence into words
for word in sentence.split(' '):
    # It will check whether word is available in frequency dictionary or not.
    if word in frequency.keys():
        # if word is available in frequency dictionary then,
        # its value(frequency) will be updated by 1.
        frequency[word] += 1
    else:
        # if word is not available in frequency dictionary then,
        # it will add that word and 1(frequency) as its value in dictionary.
        frequency[word] = 1

for word in frequency.keys():
    print(word, frequency[word])
