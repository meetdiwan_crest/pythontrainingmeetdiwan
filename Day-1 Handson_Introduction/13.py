'''
13. Paresh owns a company that moves containers  between two islands.
He has N trips booked, and each trip  has P containers.
Paresh has M boats for transporting containers,
and each boat's maximum capacity is C  containers.

Given the number of containers going on each trip,
determine whether or not Paresh can perform all  trips using
no more than boats per individual trip. If this is possible,
print Yes; otherwise, print No.

Input Format
The first line contains three space-separated integers
describing the respective values of  N(number of trips),
C (boat capacity), and  M(total number of boats).
The second line contains  space-separated integers describing
    the value for container for each trip.
Constraints
* 1<= m,c ,p<=100

Output Format
Print Yes if Paresh can perform booked trips using no more than boats per trip
otherwise, print No.
 Sample Input

0

5 2 2

1 2 1 4 3

Sample Output

0

Yes

Sample Input

0

5 1 2

1 2 1 4 3

Sample Output

0

No
'''
trips, boat_capacity, boats = input().split(' ')

# Inputs are string. So, converting it to integer
trips, boat_capacity, boats = int(trips), int(boat_capacity), int(boats)

# taking cotainer per ship as input
containers_per_trips = input().split(' ')

# converting conter per ship to integer
for i in range(len(containers_per_trips)):
    containers_per_trips[i] = int(containers_per_trips[i])

# Max containers can be shipped is boat capacity * total boats
max_containers = boat_capacity*boats

'''flag is track whether containers per trip is greater than
   max_containers capacity or not.'''
flag = 1
for i in containers_per_trips:
    if i > max_containers:
        flag = 0
        break

if(flag == 1):
    print('Yes')
else:
    print('No')
