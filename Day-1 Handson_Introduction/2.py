'''
2. Write python script to take one integer argument and then print as follows:
    - If Value >0 and Value < 10 — Small
    - If Value > 10 and Value <100 — Medium
    - If Value <1000 — Large
    - If Value > 1000 — Invalid
'''

input_number = int(input("Enter any number"))

if (input_number >= 0 and input_number < 10):
    print("Small")
elif (input_number >= 10 and input_number < 100):
    print("Medium")
elif (input_number < 1000):
    print("Large")
elif(input_number > 1000):
    print("Invalid")
