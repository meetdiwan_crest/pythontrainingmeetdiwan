'''
5. Write a program to take two integers  as input.
    Print those two integers as output
    and then call a function to swap those two integers.
- Write function for each possible way to swap two integers
'''
x, y = input("Enter two numbers: ").split(' ')
x, y = int(x), int(y)


def swap1(x, y):  # without using temperory variable
    x, y = y, x
    print("No 1: {} & No 2: {}".format(x, y))


swap1(x, y)


def swap2(x, y):  # using temperory variable
    temp = x
    x = y
    y = temp
    print("No 1: {} & No 2: {}".format(x, y))


swap2(x, y)
