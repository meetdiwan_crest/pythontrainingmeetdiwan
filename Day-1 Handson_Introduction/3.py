# 3. Write a function to find larger of three numbers.
# - Functions for each possible way we can find larger of three numbers.

number_1 = input("Enter number1: ")
number_2 = input("Enter number2: ")
number_3 = input("Enter number3: ")
largest_number = 0

if(number_1 > number_2 and number_1 > number_3):
    largest_number = number_1
elif(number_2 > number_1 and number_2 > number_3):
    largest_number = number_2
else:
    largest_number = number_3

print("Largest number = ", largest_number)
