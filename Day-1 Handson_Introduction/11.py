'''
11. Write a program a function for ATM machine
    which takes amount as input and
    output should be number of notes of each denomination.
    The ATM has notes in following denomination : 2000, 500, 100.
    Note that the ATM machine rarely gives all notes of a single amount.
    If you enter 4000, it will give 1 2000rs, 3 500rs and 5 100rs notes
    for even distribution.
'''

amount = int(input("Enter your amount: "))

# created dictionary to maintain number notes
notes = {2000: 0, 500: 0, 100: 0}

for i in notes.keys():
    # Find the possible muner of notes can be provided from current amount
    notes[i] = int(amount / i)
    # dedecut the amount for which note has provided.
    amount -= notes[i]*i

print(notes)
