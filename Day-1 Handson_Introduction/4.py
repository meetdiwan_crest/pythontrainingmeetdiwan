'''
4.Write a program to take two numbers as input parameter and
    then ask for the arithmetic parameter to be performed.
      >>> “Enter Two numbers”
      10 45
      >>>“operations to perform “
      +
      >>> 55
'''
x, y = input("Enter two numbers: ").split(' ')
x, y = int(x), int(y)
operator = input("Enter operatoreration: ")

if(operator == '+'):
    print(x + y)
elif (operator == '-'):
    print(x - y)
elif (operator == '*'):
    print(x*y)
elif (operator == '/'):
    print(x/y)
else:
    print("Invalid choice")
