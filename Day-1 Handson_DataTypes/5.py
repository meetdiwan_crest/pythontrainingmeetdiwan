'''
5. iterate on the dictionary made in Program #3
and find the percentage of each student, store it and print it in the console.
Use iterables objects to store the values.
'''

students = {"Amy": [95, 97, 99],
            "Jake": [85, 78, 89]}
percentage = {}
for student in students.keys():
    percentage[student] = sum(students[student])/3

for student in percentage.keys():
    print("Percentage of {} is {}".format(student, percentage[student]))
