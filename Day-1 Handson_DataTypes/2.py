'''
2.  Write a program to take size of the list as input.
    Then read the integer values and store these details into list.

Output:
- The list entered by the user.
'''

size_of_list = int(input("Enter size_of_list of the list: "))
user_list = []
print("Start entering values: \n")

# Appending input taken from user to the list
for i in range(size_of_list):
    inp = int(input())
    user_list.append(inp)

print(user_list)
