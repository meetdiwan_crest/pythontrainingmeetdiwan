'''
3. Write a program that takes Student's name
    and their marks in 3 subjects as input.
   Print each student's total marks as output.
'''

total_students = int(input("Enter student's count: "))
students = {}
for i in range(total_students):
    student_name = input("Enter student's name: ")
    # It will maintain list of 3 marks
    marks = []
    # To take marks from input.
    for j in range(3):
        result = int(input("Enter marks in subject {}: ".format(j+1)))
        marks.append(result)
    # It will create new elemen in student dictionary.
    # With key as student name & marks list as value
    students[student_name] = marks

for student in students.keys():
    print("{}'s total marks = {}".format(student, sum(students[student])))
