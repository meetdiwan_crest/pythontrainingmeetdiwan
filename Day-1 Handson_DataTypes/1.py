'''
1.  Saurabh needs to withdraw X Rs. from an ATM.
    The transaction will succeed only if X is an odd number,
    and Saurabh's account balance has enough cash to perform
    the withdrawal transaction(including bank charges).
    For each successful withdrawal the bank charges 10.50 Rs.
    Calculate Saurabh's account balance after an attempted transaction.

Input:
- Saurabh's initial account balance
- Withdrawal amount

Output
- Amount present in Saurabh's account after withdrawal.
- Error message, if the withdrawal did not match transaction criteria.
'''

initial_amount = float(input("Enter initial amount: "))

# As withdrawal amount must be odd, it should be integer.
withdrawal_amount = int(input("Enter withdrawal amount: "))

'''
10.50 is charges for transaction, so Shaurabh must have bank balance
more than wihdrawal amount and 10.50 combined.
'''
if(withdrawal_amount % 2 == 1
   and (float(withdrawal_amount) + 10.50) <= initial_amount):
    print("Amount present in Saurabh's account after withdrawal is ",
          initial_amount - withdrawal_amount - 10.50)
else:
    print("Error, withdrawal did not match transaction criteria")
