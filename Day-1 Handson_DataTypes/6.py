'''
6. "Create a program to take student information as input.
    Student will have First Name, Last Name, Roll No.
    Write a function to sort the list based on given input parameter.
    i.e By First Name or Last Name or Roll No."
'''
student_count = int(input("Enter student's count: "))

# it will store first_name, last_name & age as dictionary.
student_dataset = []
for i in range(student_count):
    student_info = {}
    student_info['first_name'] = input("Enter first name: ")
    student_info['last_name'] = input("Enter last name: ")
    student_info['Roll_no'] = input("Enter Roll no: ")
    student_dataset.append(student_info)


# User's choice of field to sort the data.
choice = int(input('''Enter your choice for sorting the data:
1) First name 2) Last name 3) Roll no. :'''))


def sorting_key(list1):
    if choice == 1:
        return list1['first_name']
    elif choice == 2:
        return list1['last_name']
    else:
        return list1['Roll_no']


if(choice not in [1, 2, 3]):
    print("Invalid choice")
else:
    # This fucntion will sort the data according to the choice.
    student_dataset.sort(key=sorting_key)
    print(student_dataset)
