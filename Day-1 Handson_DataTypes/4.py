'''
4. Write a program that takes the input from the user (i.e., N). Create the

generator function that takes this input as an argument
and returns numbers from 1 to N.

Output:
- Using the generator function, print the numbers from 1 to N.
'''


def generator_function(n):  # generator fucntion to return numbers from 1 to N
    for i in range(1, n+1):
        yield i


n = int(input("Enter value of N: "))
output_generator = generator_function(n)

for values in output_generator:
    print(values)
